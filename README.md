# Тех задание:
1. Напишите ansible роль, которая будет устанавливать и настраивать nginx на сервере.
2. Напишите конфигурацию виртуального хоста nginx, которая должна принимать запросы для test1.example.io на 80 порт. 
   С 80 порта запросы должны постоянным редиректом перенаправлены на 443 https.
3. Напишите Dockerfile, который будет собирать nginx с измененным параметром логирования в nginx.conf
4. Напишите простой helmChart, который будет деплоить nginx в качестве front части. В качестве upstream использовать https://ifconfig.io. HelmChart должен содержать шаблоны deployment, configMap, ingress.


## Задача 1
Запуск vagrant и установка nginx
```
cd ./instance && vagrant up && cd ..
cd ./tasks_nginx && ansible-playbook nginx.yml && cd ..
```

## Задача 2
Добавление конфигурации, выполняется только после шага 1
Для проверки править локально /etc/hosts добавить
xxx.xxx.xxx.xxx   test1.example.io
```
cd ./tasks_nginx && ansible-playbook vhost.yml && cd ..
```

## Задача 3
Сборка проекта с готовым конфиг файлом
Как вариант можно использовать и sed, но значительно менее очевидный вариант
```
cd ./task_docker_log && docker build . && cd ..
```

## Задача 4
```
helm install testapp task_helm/
helm upgrade testapp task_helm/
```
